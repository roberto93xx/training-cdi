0. Scaricare JBOSS EAP 7.1
1. Scaricare la seguente libreria:  mysql-connector-java-5.1.47.jar
2. Deployare su JBOSS EAP 7.1 la libreria: mysql-connector-java-5.1.47.jar
	2_1. Aprire la console di admin di JBOSS EAP 7.1: http://localhost:9990/console (Nota richiede utenza)
	2_2. Tab Deployments -> Add -> scegli file -> selezionare la libreria scaricata in precedenza: mysql-connector-java-5.1.47.jar
3. Creare un datasource con la libreria scaricata
	3_1. Tab Configurations -> Subsystems -> Datasources -> Non-XA -> Add -> MySqlDatasources -> Dare nome e nome JNDI al Datasource ->
	     Detected Driver -> Scegli il driver deployato prima: mysql-connector-java-5.1.47.jar -> Compilare dati Connection Settings 