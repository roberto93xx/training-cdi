CREATE DATABASE IF NOT EXISTS shopify; 
USE shopify;

-- ---------------------------------
-- -- START DDL SCHEMA DEFINITION --
-- ---------------------------------
CREATE TABLE CATEGORY (
	-- surrogate pk
	ID 		INT 		 AUTO_INCREMENT,
    NAME 	VARCHAR(255) NOT NULL,
    
    PRIMARY KEY(ID)
);

CREATE TABLE PRODUCT (
	-- surrogate pk
    ID 			 INT 			   AUTO_INCREMENT,
    NAME	     VARCHAR(255)      NOT NULL UNIQUE,
	DESCRIPTION  VARCHAR(255)      NOT NULL,
    PRICE    	 NUMERIC(4,2),
    ID_CATEGORY	 INT	 		   NOT NULL,
    
    PRIMARY KEY(ID),
    FOREIGN KEY(ID_CATEGORY) REFERENCES CATEGORY(ID)
);

CREATE TABLE USER(
	-- surrogate pk
    ID 			 INT 			   AUTO_INCREMENT,
    USERNAME	 VARCHAR(255) 	   NOT NULL,
    PASSWORD	 VARCHAR(255) 	   NOT NULL,
    
    PRIMARY KEY(ID)

);
-- -------------------------------
-- -- END DDL SCHEMA DEFINITION --
-- -------------------------------


-- -------------------------------
-- ------ START INSERT DATA ------
-- -------------------------------

-- INSERT CATEGORY
INSERT INTO CATEGORY(id, name) VALUES(1, 'Dress');
INSERT INTO CATEGORY(id, name) VALUES(2, 'Games');
INSERT INTO CATEGORY(id, name) VALUES(3, 'Shoes');

-- INSERT PRODUCT
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (1, 'Red t-shirt', 'Fantasctic red t-shirt available in size XS/S/M/L/XL/XXL', 19.99, 1);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (2, 'Pink Jacket', 'Wonderful pink jacket for every event available in size S/M/XXL', 61.37, 1);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (3, 'Goalkeeper gloves', 'Best gooalkeeper gloves of ever avalaible in size S/M/L/XL', 30.98, 1);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (4, 'Super Mario Bros',  'Best vintage game of every years', 60.00, 2);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (5, 'Football Manager',  'Good Football game for fun and help to win your favorite team', 50.49, 2);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (6, 'Call of duty', 'Best war game in the world', 70.18, 2);
INSERT INTO PRODUCT(id, name, description, price, id_category) VALUES (7, 'Football shoes', 'CR7 shoes ', 90.98, 3);


-- -------------------------------
-- ------- END INSERT DATA -------
-- -------------------------------