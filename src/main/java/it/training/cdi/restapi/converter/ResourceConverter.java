package it.training.cdi.restapi.converter;

import java.util.List;

public interface ResourceConverter<DBEntity, Resource> {
	public Resource convertAtoBLazy(DBEntity a);
	public Resource convertAtoBEager(DBEntity a);
	
	public List<Resource> convertAtoBLazy(List<DBEntity> a);
	public List<Resource> convertAtoBEager(List<DBEntity> aList);
}
