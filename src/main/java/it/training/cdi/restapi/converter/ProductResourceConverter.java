package it.training.cdi.restapi.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import it.training.cdi.entity.Product;
import it.training.cdi.restapi.resource.ProductEagerResource;
import it.training.cdi.restapi.resource.ProductLazyResource;
import it.training.cdi.restapi.resource.ProductResource;

public class ProductResourceConverter implements ResourceConverter<Product, ProductResource>{

	@Override
	public ProductResource convertAtoBLazy(Product a) {
		if(a == null) {
			return null;
		}
		ProductLazyResource plr = new ProductLazyResource();
		plr.setId(a.getId());
		plr.setName(a.getName());
		plr.setDescription(a.getDescription());
		plr.setPrice(a.getPrice());
		
		return plr;
	}

	@Override
	public ProductResource convertAtoBEager(Product a) {
		if(a == null) {
			return null;
		}
		ProductEagerResource per = new ProductEagerResource();
		per.setId(a.getId());
		per.setName(a.getName());
		per.setDescription(a.getDescription());
		per.setPrice(a.getPrice());
		per.setCategory(a.getCategory());
		
		return per;
	}

	@Override
	public List<ProductResource> convertAtoBLazy(List<Product> aList) {
		if(aList == null) {
			return null;
		}
		if(aList.isEmpty()) {
			return new ArrayList<>();
		}
		
		return aList.stream()
					.map( this::convertAtoBLazy )
					.collect(Collectors.toList());
	}

	@Override
	public List<ProductResource> convertAtoBEager(List<Product> aList) {
		if(aList == null) {
			return null;
		}
		if(aList.isEmpty()) {
			return new ArrayList<>();
		}
		
		return aList.stream()
				.map( this::convertAtoBEager )
				.collect(Collectors.toList());
	}
}