package it.training.cdi.restapi.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import it.training.cdi.entity.Category;
import it.training.cdi.restapi.resource.CategoryEagerResource;
import it.training.cdi.restapi.resource.CategoryLazyResource;
import it.training.cdi.restapi.resource.CategoryResource;

public class CategoryResourceConverter implements ResourceConverter<Category, CategoryResource>{

	@Override
	public CategoryResource convertAtoBLazy(Category a) {
		if(a == null) {
			return null;
		}
		CategoryLazyResource clr = new CategoryLazyResource();
		clr.setId(a.getId());
		clr.setName(a.getName());
		
		return clr;
	}

	@Override
	public CategoryResource convertAtoBEager(Category a) {
		if(a == null) {
			return null;
		}
		CategoryEagerResource cer = new CategoryEagerResource();
		cer.setId(a.getId());
		cer.setName(a.getName());
		cer.setProducts(a.getProducts());
		
		return cer;
	}

	@Override
	public List<CategoryResource> convertAtoBLazy(List<Category> aList) {
		if(aList == null) {
			return null;
		}
		if(aList.isEmpty()) {
			return new ArrayList<>();
		}
		
		return aList.stream()
					.map( this::convertAtoBLazy )
					.collect(Collectors.toList());
	}

	@Override
	public List<CategoryResource> convertAtoBEager(List<Category> aList) {
		if(aList == null) {
			return null;
		}
		if(aList.isEmpty()) {
			return new ArrayList<>();
		}
		
		return aList.stream()
				.map( this::convertAtoBEager )
				.collect(Collectors.toList());
	}
}