package it.training.cdi.restapi.conf;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "/rest/api/v1")
public class RestApiConfiguration extends Application{ }