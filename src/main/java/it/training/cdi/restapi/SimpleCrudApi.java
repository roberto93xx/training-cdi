package it.training.cdi.restapi;

import javax.ws.rs.core.Response;

public interface SimpleCrudApi<Resource> {

	public Response getAll(String lazyMode);
	
	public Response getById(Long id, String lazyMode);
}
