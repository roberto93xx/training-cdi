package it.training.cdi.restapi.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.training.cdi.ProductResConverter;
import it.training.cdi.ProductServiceEager;
import it.training.cdi.ProductServiceLazy;
import it.training.cdi.entity.Product;
import it.training.cdi.restapi.SimpleCrudApi;
import it.training.cdi.restapi.converter.ResourceConverter;
import it.training.cdi.restapi.resource.ProductResource;
import it.training.cdi.service.ProductService;

@Path(value = "/product")
@Produces(value = "application/json")
@Consumes(value = "application/json")
public class ProductRestApi implements SimpleCrudApi<ProductResource> {
	@Inject 
	private Logger LOG;
	@Inject 
	@ProductServiceLazy
	private ProductService productServiceLazy;
	@Inject
	@ProductServiceEager 
	private ProductService productServiceEager;
	@Inject
	@ProductResConverter
	private ResourceConverter<Product, ProductResource> productResourceConverter;

	@GET
	@Override
	public Response getAll(@QueryParam(value = "lazyMode") String lazyMode){
		LOG.info(String.format(">>> GET rest/api/v1/product - getAll  lazyMode=%s - START", lazyMode));
		List<Product> productEntityList = null;
		List<ProductResource> productResourceList = null;
		if(lazyMode == null || lazyMode.trim().isEmpty() || !lazyMode.equalsIgnoreCase("false")) {
			productEntityList = this.productServiceLazy.getAll();
			productResourceList = this.productResourceConverter.convertAtoBLazy(productEntityList);
		} else {
			productEntityList = this.productServiceEager.getAll();
			productResourceList = this.productResourceConverter.convertAtoBEager(productEntityList);
		}
		LOG.info("getAll - END");
		return Response.status(Status.OK).entity(productResourceList).build();
	}

	@GET
	@Path(value = "/{id}")
	@Override
	public Response getById(@PathParam(value = "id") Long id, @QueryParam(value = "lazyMode") String lazyMode) {
		LOG.info(String.format(">>> GET rest/api/v1/product/%s - getById  id=%s - lazyMode=%s - START", id, id ,lazyMode));
		Product productEntity = null;
		ProductResource productResource = null;
		if(lazyMode == null || lazyMode.trim().isEmpty() || !lazyMode.equalsIgnoreCase("false")) {
			productEntity = this.productServiceLazy.getById(id);
			productResource = this.productResourceConverter.convertAtoBLazy(productEntity);
		} else {
			productEntity = this.productServiceEager.getById(id);
			productResource = this.productResourceConverter.convertAtoBEager(productEntity);
		}

		Response responseToClient = null;
		if(productEntity == null) {
			responseToClient = Response.status(Status.NOT_FOUND).build();
		} else {
			responseToClient = Response.status(Status.NOT_FOUND).entity(productResource).build();
		}
		LOG.info("getById - END");
		return responseToClient;
	}
}