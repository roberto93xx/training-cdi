package it.training.cdi.restapi.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.training.cdi.CategoryResConverter;
import it.training.cdi.CategoryServiceEager;
import it.training.cdi.CategoryServiceLazy;
import it.training.cdi.entity.Category;
import it.training.cdi.restapi.SimpleCrudApi;
import it.training.cdi.restapi.converter.ResourceConverter;
import it.training.cdi.restapi.resource.CategoryResource;
import it.training.cdi.service.CategoryService;

@Path(value = "/category")
@Produces(value = "application/json")
@Consumes(value = "application/json")
public class CategoryRestApi implements SimpleCrudApi<CategoryResource> {
	@Inject 
	private Logger LOG;
	@Inject 
	@CategoryServiceLazy
	private CategoryService categoryServiceLazy;
	@Inject
	@CategoryServiceEager 
	private CategoryService categoryServiceEager;
	@Inject
	@CategoryResConverter
	private ResourceConverter<Category, CategoryResource> categoryResorceConverter;
	
	@GET
	@Override
	public Response getAll(@QueryParam(value = "lazyMode") String lazyMode){
		LOG.info(String.format(">>> GET rest/api/v1/category - getAll  lazyMode=%s - START", lazyMode));
		List<Category> cateforyEntityList = null;
		List<CategoryResource> productResourceList = null;
		if(lazyMode == null || lazyMode.trim().isEmpty() || !lazyMode.equalsIgnoreCase("false")) {
			cateforyEntityList = this.categoryServiceLazy.getAll();
			productResourceList = this.categoryResorceConverter.convertAtoBLazy(cateforyEntityList);
		} else {
			cateforyEntityList = this.categoryServiceEager.getAll();
			productResourceList = this.categoryResorceConverter.convertAtoBEager(cateforyEntityList);
		}
		LOG.info("getAll - END");
		return Response.status(Status.OK).entity(productResourceList).build();
	}

	@GET
	@Path(value = "/{id}")
	@Override
	public Response getById(@PathParam(value = "id") Long id, @QueryParam(value = "lazyMode") String lazyMode) {
		LOG.info(String.format(">>> GET rest/api/v1/category/%s - getById  id=%s - lazyMode=%s - START", id, id ,lazyMode));
		Category categoryEntity = null;
		CategoryResource categoryResource = null;
		if(lazyMode == null || lazyMode.trim().isEmpty() || !lazyMode.equalsIgnoreCase("false")) {
			categoryEntity = this.categoryServiceLazy.getById(id);
			categoryResource = this.categoryResorceConverter.convertAtoBLazy(categoryEntity);
		} else {
			categoryEntity = this.categoryServiceEager.getById(id);
			categoryResource = this.categoryResorceConverter.convertAtoBEager(categoryEntity);
		}

		Response responseToClient = null;
		if(categoryEntity == null) {
			responseToClient = Response.status(Status.NOT_FOUND).build();
		} else {
			responseToClient = Response.status(Status.NOT_FOUND).entity(categoryResource).build();
		}
		LOG.info("getById - END");
		return responseToClient;
	}
}