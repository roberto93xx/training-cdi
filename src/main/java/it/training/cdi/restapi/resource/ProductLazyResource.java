package it.training.cdi.restapi.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductLazyResource extends ProductResource {
	private static final long serialVersionUID = -8123776194469474058L;

	@JsonProperty(value = "name")
	private String name;
	@JsonProperty(value = "description")
	private String description;
	@JsonProperty(value = "price")
	private Float price;

	public ProductLazyResource() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductLazyResource [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", price=");
		builder.append(price);
		builder.append(", id=");
		builder.append(getId());
		builder.append("]");
		return builder.toString();
	}

}
