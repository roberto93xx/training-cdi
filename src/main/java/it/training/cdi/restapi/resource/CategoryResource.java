package it.training.cdi.restapi.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class CategoryResource extends Resource {
	private static final long serialVersionUID = -4967137397413539899L;

	@JsonProperty(value = "id")
	private Long id;

	public CategoryResource() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
