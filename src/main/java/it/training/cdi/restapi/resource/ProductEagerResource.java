package it.training.cdi.restapi.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.training.cdi.entity.Category;

public class ProductEagerResource extends ProductLazyResource {
	private static final long serialVersionUID = -8123776194469474058L;

	@JsonProperty(value = "category")
	private Category category;

	public ProductEagerResource() {
		super();
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}