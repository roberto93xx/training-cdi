package it.training.cdi.restapi.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ProductResource extends Resource {
	private static final long serialVersionUID = -8123776194469474058L;

	@JsonProperty(value = "id")
	private Long id;

	public ProductResource() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
