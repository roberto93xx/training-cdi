package it.training.cdi.restapi.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.training.cdi.entity.Product;

public class CategoryEagerResource extends CategoryLazyResource {
	private static final long serialVersionUID = -8123776194469474058L;

	@JsonProperty(value = "products")
	private List<Product> products;

	public CategoryEagerResource() {
		super();
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
}