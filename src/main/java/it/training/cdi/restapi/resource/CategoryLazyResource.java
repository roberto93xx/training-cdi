package it.training.cdi.restapi.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryLazyResource extends CategoryResource {
	private static final long serialVersionUID = -8123776194469474058L;

	@JsonProperty(value = "name")
	private String name;

	public CategoryLazyResource() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}