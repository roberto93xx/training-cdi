package it.training.cdi.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "CATEGORY")
@NamedQueries(value = {
		@NamedQuery(name = "getAllCategory", 					query = "SELECT c FROM Category c"),
		@NamedQuery(name = "getAllCategoryEager", 				query = "SELECT c FROM Category c JOIN FETCH c.products"),
		@NamedQuery(name = "getCategoryById", 					query = "SELECT c FROM Category c WHERE c.id =:theId"),
		@NamedQuery(name = "getCategoryByIdEager", 				query = "SELECT c FROM Category c JOIN FETCH c.products WHERE c.id =:theId"),
})
public class Category extends DBEntity {
	private static final long serialVersionUID = -4199030074104687521L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "NAME")
	@NotNull
	@Size(min = 1, message = "name cannot be blank.")
	private String name;
	@JsonIgnore
	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY, targetEntity = Product.class)
	private List<Product> products;

	public Category() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		if(this.products == null) {
			this.products = new ArrayList<Product>();
			return ;
		}
		this.products = products;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Category [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

}