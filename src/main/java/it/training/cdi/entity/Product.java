package it.training.cdi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PRODUCT")
@NamedQueries(value = {
			@NamedQuery(name = "getAllProduct", 					query = "SELECT p FROM Product p"),
			@NamedQuery(name = "getAllProductEager", 				query = "SELECT p FROM Product p JOIN FETCH p.category"),
			@NamedQuery(name = "getProductById", 					query = "SELECT p FROM Product p WHERE p.id =:theId"),
			@NamedQuery(name = "getProductByIdEager", 				query = "SELECT p FROM Product p JOIN FETCH p.category WHERE p.id =:theId"),
			@NamedQuery(name = "getProductByDescriptionLike",  	query = "SELECT p FROM Product p WHERE p.description LIKE '%:=theDescription%'"),
			@NamedQuery(name = "getProductByDescriptionLikeEager", query = "SELECT p FROM Product p JOIN FETCH p.category WHERE p.description LIKE '%:=theDescription%'")
})
public class Product extends DBEntity {
	private static final long serialVersionUID = -3649214819167720508L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "NAME", unique = true)
	@NotNull(message = "name cannot be null")
	@Size(min = 1, message = "name cannot be blank.")
	private String name;
	@Column(name = "DESCRIPTION")
	@NotNull(message = "description cannot be null")
	@Size(min = 10, message = "description must have at least 10 character.")
	private String description;
	@Column(name = "PRICE")
	private Float price;
	@JsonIgnore
	@DecimalMin(value = "0.01", inclusive = true, message = "price must be value at least 0.01.")
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Category.class)
	@JoinColumn(name = "ID_CATEGORY")
	private Category category;

	public Product() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", price=");
		builder.append(price);
		builder.append("]");
		return builder.toString();
	}
}