package it.training.cdi;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.training.cdi.entity.Category;
import it.training.cdi.entity.Product;
import it.training.cdi.restapi.converter.CategoryResourceConverter;
import it.training.cdi.restapi.converter.ProductResourceConverter;
import it.training.cdi.restapi.converter.ResourceConverter;
import it.training.cdi.restapi.resource.CategoryResource;
import it.training.cdi.restapi.resource.ProductResource;

public class ManagedBeans {
	@PersistenceContext(name = "ShopifyPU")
	@Produces
	private EntityManager entityManager;
	
	@Produces
	private Logger getLogger(InjectionPoint injectionPoint) {
		String className = injectionPoint.getMember().getDeclaringClass().getSimpleName();
		Logger LOG = Logger.getLogger(className);
		return LOG;
	}
	
	@Produces
	@ApplicationScoped
	@ProductResConverter
	private ResourceConverter<Product, ProductResource> getProductResourceConverter() {
		return new ProductResourceConverter();
	}
	
	@Produces
	@ApplicationScoped
	@CategoryResConverter
	private ResourceConverter<Category, CategoryResource> getCaetgoryResourceConverter() {
		return new CategoryResourceConverter();
	}
	
}