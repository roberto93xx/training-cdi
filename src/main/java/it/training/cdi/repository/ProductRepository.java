package it.training.cdi.repository;

import java.util.List;

import it.training.cdi.entity.Product;

public interface ProductRepository extends CrudRepository<Product>{
	public List<Product> getByPriceMajorOf(float priceCompare);
}
