package it.training.cdi.repository.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import it.training.cdi.CategoryRepositoryLazy;
import it.training.cdi.entity.Category;
import it.training.cdi.repository.CategoryRepository;

@ApplicationScoped
@CategoryRepositoryLazy
public class CategoryRepositoryImpl1 implements CategoryRepository {
	@Inject
	private Logger LOG;
	@Inject
	private EntityManager entityManager;
	
	@Override
	public List<Category> getAll() {
		LOG.info("getAll - START");
		TypedQuery<Category> query = this.entityManager.createNamedQuery("getAllCategory", Category.class);
		List<Category> categoriesResult = query.getResultList();		
		LOG.info("getAll - END");
		return categoriesResult;
	}

	@Override
	public Category getById(Long id) {
		LOG.info(String.format("getById - id=%s - START", id));
		TypedQuery<Category> query = this.entityManager.createNamedQuery("getCategoryById", Category.class);
		query.setParameter("theId",id);
		List<Category> categoriesResult = query.getResultList();
		Category categoryResult = null;
		if(categoriesResult != null && !categoriesResult.isEmpty()) {
			categoryResult = categoriesResult.get(0);
		} 
		LOG.info("getById - END");
		return categoryResult;
	}

	@Override
	public boolean save(Category category) {
		LOG.info(String.format("save - %s - START", category));
		// TODO Auto-generated method stub
		LOG.info("save - END");
		return false;
	}

	@Override
	public void update(Category category) {
		LOG.info(String.format("update - %s - START", category));
		// TODO Auto-generated method stub
		LOG.info("update - END");
	}

	@Override
	public void delete(Category category) {
		LOG.info(String.format("delete - %s - START", category));
		// TODO Auto-generated method stub
		LOG.info("delete - END");
	}
	
	@Override
	public Category getByName(String name) {
		LOG.info(String.format("getByName - name=%s - START", name));
		// TODO Auto-generated method stub
		LOG.info("getByName - END");
		return null;
	}
}