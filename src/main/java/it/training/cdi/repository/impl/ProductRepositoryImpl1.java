package it.training.cdi.repository.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import it.training.cdi.ProductRepositoryLazy;
import it.training.cdi.entity.Product;
import it.training.cdi.repository.ProductRepository;

@ApplicationScoped
@ProductRepositoryLazy
public class ProductRepositoryImpl1 implements ProductRepository {
	@Inject
	private  Logger LOG;
	@Inject
	private EntityManager entityManager;
	
	@Override
	public List<Product> getAll() {
		LOG.info("getAll - START");
		TypedQuery<Product> query = this.entityManager.createNamedQuery("getAllProduct", Product.class);
		List<Product> productsResult = query.getResultList();
		LOG.info("getAll - END");
		return productsResult;
	}

	@Override
	public Product getById(Long id) {
		LOG.info(String.format("getById - id=%s - START", id));
		TypedQuery<Product> query = this.entityManager.createNamedQuery("getProductById", Product.class);
		query.setParameter("theId",id);
		List<Product> productsResult = query.getResultList();
		Product productResult = null;
		if(productsResult != null && !productsResult.isEmpty()) {
			productResult = productsResult.get(0);
		} 
		LOG.info("getById - END");
		return productResult;
	}

	@Override
	public boolean save(Product product) {
		LOG.info(String.format("save - %s - START", product));
		// TODO Auto-generated method stub
		LOG.info("save - END");
		return false;
	}

	@Override
	public void update(Product product) {
		LOG.info(String.format("update - %s - START", product));
		// TODO Auto-generated method stub
		LOG.info("update - END");
		
	}

	@Override
	public void delete(Product product) {
		LOG.info(String.format("delete - %s - START", product));
		// TODO Auto-generated method stub
		LOG.info("delete - END");
	}
	
	@Override
	public List<Product> getByPriceMajorOf(float priceCompare) {
		LOG.info(String.format("getByPriceMajorOf - priceCompare=%s - START", priceCompare));
		// TODO Auto-generated method stub
		LOG.info("getByPriceMajorOf - END");
		return null;
	}
}