package it.training.cdi.repository;

import it.training.cdi.entity.Category;

public interface CategoryRepository extends CrudRepository<Category>{
	public Category getByName(String name);
}
