package it.training.cdi.repository;

import java.util.List;

import it.training.cdi.entity.DBEntity;

public interface CrudRepository<T extends DBEntity> {

	public List<T> getAll();
	
	public T getById(Long id);
	
	public boolean save(T entity);
	
	public void update(T entity);

	void delete(T entity);
}
