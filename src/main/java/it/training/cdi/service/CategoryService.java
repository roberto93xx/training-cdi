package it.training.cdi.service;

import java.util.List;

import it.training.cdi.entity.Category;

public interface CategoryService {

	public List<Category> getAll();
	
	public Category getById(Long id);
}
