package it.training.cdi.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

import it.training.cdi.CategoryRepositoryLazy;
import it.training.cdi.CategoryServiceLazy;
import it.training.cdi.entity.Category;
import it.training.cdi.repository.CategoryRepository;
import it.training.cdi.service.CategoryService;
import it.training.cdi.service.CrudService;

@Stateless
@Local
@CategoryServiceLazy
public class CategoryServiceImpl1 implements CrudService<Category>, CategoryService {
	@Inject
	private Logger LOG;
	@Inject
	@CategoryRepositoryLazy
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> getAll() {
		LOG.info("getAll - START");
		List<Category> caetgoriesResult = this.categoryRepository.getAll();
		LOG.info("getAll - END");
		return caetgoriesResult;
	}

	@Override
	public Category getById(Long id) {
		LOG.info(String.format("getById - id=%s - START", id));
		Category category = this.categoryRepository.getById(id);
		LOG.info("getById - END");
		return category;
	}

	@Override
	public boolean save(Category category) {
		LOG.info(String.format("save - %s - START", category));
		// TODO Auto-generated method stub
		LOG.info("save - END");

		return false;
	}

	@Override
	public void update(Long idToUpd, Category categoryToUpd) {
		LOG.info(String.format("update - idToUpd=%s, %s - START", idToUpd, categoryToUpd));
		// TODO Auto-generated method stub
		LOG.info("update - END");
	}

	@Override
	public void delete(Long idToDel) {
		LOG.info(String.format("delete - idToDel=%s - START",idToDel));
		// TODO Auto-generated method stub
		LOG.info("delete - END");
	}
	
	
}
