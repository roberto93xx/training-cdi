package it.training.cdi.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

import it.training.cdi.CategoryRepositoryEager;
import it.training.cdi.CategoryServiceEager;
import it.training.cdi.entity.Category;
import it.training.cdi.repository.CategoryRepository;
import it.training.cdi.service.CategoryService;
import it.training.cdi.service.CrudService;

@Stateless
@Local
@CategoryServiceEager
public class CategoryServiceImpl2 implements CrudService<Category>, CategoryService {
	@Inject
	private Logger LOG;
	@Inject
	@CategoryRepositoryEager
	private CategoryRepository categoryRepository;

	@Override
	public List<Category> getAll() {
		LOG.info("getAll - START");
		List<Category> categoriesResult = this.categoryRepository.getAll();
		LOG.info("getAll - END");
		return categoriesResult;
	}

	@Override
	public Category getById(Long id) {
		LOG.info(String.format("getById - id=%s - START", id));
		Category category = this.categoryRepository.getById(id);
		LOG.info("getById - END");
		return category;
	}

	@Override
	public boolean save(Category category) {
		LOG.info(String.format("save - %s - START", category));
		// TODO Auto-generated method stub
		LOG.info("save - END");

		return false;
	}

	@Override
	public void update(Long idToUpd, Category categoryToUpd) {
		LOG.info(String.format("update - idToUpd=%s, %s - START", idToUpd, categoryToUpd));
		// TODO Auto-generated method stub
		LOG.info("update - END");
	}

	@Override
	public void delete(Long idToDel) {
		LOG.info(String.format("delete - idToDel=%s - START",idToDel));
		// TODO Auto-generated method stub
		LOG.info("delete - END");
	}
	
	
}
