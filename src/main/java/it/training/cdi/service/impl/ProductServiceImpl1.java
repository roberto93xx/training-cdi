package it.training.cdi.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;

import it.training.cdi.ProductRepositoryLazy;
import it.training.cdi.ProductServiceLazy;
import it.training.cdi.entity.Product;
import it.training.cdi.repository.ProductRepository;
import it.training.cdi.service.ProductService;

@Stateless
@Local
@ProductServiceLazy
public class ProductServiceImpl1 implements ProductService {
	@Inject
	private  Logger LOG;
	@Inject
	@ProductRepositoryLazy
	private ProductRepository productRepository;
	
	@Override
	public List<Product> getAll() {
		LOG.info("getAll - START");
		List<Product> productsResult = this.productRepository.getAll();
		LOG.info("getAll - END");
		return productsResult;
	}

	@Override
	public Product getById(Long id) {
		LOG.info(String.format("getById - id=%s - START", id));
		Product product = this.productRepository.getById(id);
		LOG.info("getById - END");
		return product;
	}

	@Override
	public boolean save(Product product) {
		LOG.info(String.format("save - %s - START", product));
		// TODO Auto-generated method stub
		LOG.info("save - END");
		return false;
	}

	@Override
	public void update(Long idToUpd, Product productToUpd) {
		LOG.info(String.format("update - idToUpd=%s, %s - START", idToUpd, productToUpd));
		// TODO Auto-generated method stub
		LOG.info("update - END");
	}

	@Override
	public void delete(Long idToDel) {
		LOG.info(String.format("delete - idToDel=%s - START",idToDel));
		// TODO Auto-generated method stub
		LOG.info("delete - END");
	}
}