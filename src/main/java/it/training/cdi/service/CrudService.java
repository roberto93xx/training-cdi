package it.training.cdi.service;

import java.util.List;

import it.training.cdi.entity.DBEntity;

public interface CrudService<T extends DBEntity> {
	public List<T> getAll();
	
	public T getById(Long id);
	
	public boolean save(T entity);
	
	public void update(Long idToUpd, T entityToUpd);
	
	public void delete(Long idToDel);
}
