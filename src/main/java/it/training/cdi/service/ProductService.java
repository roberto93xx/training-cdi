package it.training.cdi.service;

import it.training.cdi.entity.Product;

public interface ProductService extends CrudService<Product> {
}
